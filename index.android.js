import {
    View,
    Text,
    TextInput,
    Dimensions,
    AppRegistry,
    Alert
} from 'react-native';
import React, { Component } from 'react';

const deviceWidth = require('Dimensions').get('window').width;
const deviceHeight = require('Dimensions').get('window').height;

const PasscodeTextInput = ({ ref, inputRef, autoFocus, onSubmitEditing, onChangeText, value, returnKeyType, blurOnSubmit }) => {

    const { inputStyle, underlineStyle } = styles;

    return (
        <View>
            <TextInput
                ref={(r) => { inputRef && inputRef(r) }}
                autoFocus={autoFocus}
                onSubmitEditing={onSubmitEditing}
                style={[inputStyle]}
                maxLength={1}
                keyboardType="numeric"
                placeholderTextColor="#212121"
                secureTextEntry={true}
                onChangeText={onChangeText}
                value={value}
                underlineColorAndroid="grey"
                returnKeyType={returnKeyType}
                blurOnSubmit={blurOnSubmit}
            />
        </View>
    );
}


export default class AwesomeProject2 extends Component {

    constructor() {
        super()
        this.state = {
            pins: []
        }
    }

    alert() {
        Alert.alert('pin sent', this.state.pins.toString());
    }

    setStateAndFocus(text, nextPassCodeRef, prevPassCodeRef) {
        this.setState({
            pins: [...this.state.pins, text]
        })
        text ? nextPassCodeRef && nextPassCodeRef.focus() : prevPassCodeRef && prevPassCodeRef.focus();
    }

    render() {
        const { centerEverything, container, passcodeContainer, testShit, textInputStyle } = styles;
        return (
            <View style={[centerEverything, container]}>
                <View style={[passcodeContainer]}>
                    <PasscodeTextInput
                        autoFocus={true}
                        inputRef={(r) => { this.passcode1 = r }}
                        onChangeText={(text) => { this.setStateAndFocus(text, this.passcode2) }}
                        onSubmitEditing={(text) => this.passcode2.focus()}
                        returnKeyType={'next'} />

                    <PasscodeTextInput
                        inputRef={(r) => { this.passcode2 = r }}
                        onChangeText={(text) => { this.setStateAndFocus(text, this.passcode3, this.passcode1) }}
                        returnKeyType={'next'} />

                    <PasscodeTextInput
                        inputRef={(r) => { this.passcode3 = r }}
                        onChangeText={(text) => { this.setStateAndFocus(text, this.passcode4, this.passcode2) }}
                        returnKeyType={'next'} />

                    <PasscodeTextInput
                        inputRef={(r) => { this.passcode4 = r }}
                        onChangeText={(text) => { this.setStateAndFocus(text, null, this.passcode3) }}
                        onSubmitEditing={this.alert.bind(this)} />
                </View>
            </View>
        );
    }
}

const styles = {
    centerEverything: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: '#E7DDD3',
    },
    passcodeContainer: {
        flexDirection: 'row',
    },
    inputStyle: {
        height: 80,
        width: 60,
        fontSize: 50,
        color: '#212121',
        fontSize: 40,
        padding: 20,
        margin: 10,
        marginBottom: -101
    },
    underlineStyle: {
        width: 60,
        height: 4,
        backgroundColor: '#202020',
        marginLeft: 10
    }
}

AppRegistry.registerComponent('AwesomeProject2', () => AwesomeProject2);